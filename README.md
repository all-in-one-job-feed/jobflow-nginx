# Deployment Using Docker

### Usage:

Build the image for stage by specify the enviornment:
```sh
docker-compose docker-compose.yml -f stage.yml build
```

Build the image for prod by specify the enviornment:
```sh 
docker-compose -f docker-compose.yml -f prod.yml build
```

Then start the containers:
```sh 
docker-compose up
```
add `-d` to run it in the background

After the docker container is up.

TO access the UI
For stage: go to 127.0.0.1
For prod: go to your own url

### RabbitMQ
Access the UI manager by going to port 15672.

